// es6 arrow function examples
//==================================

// arguments object - no longer bound with arrow functions

// const add = (a, b) => {
//   // cannot access arguments keyword
//   // console.log(arguments);
//   return a + b;
// };
// console.log(add(55,1,10001,234)); // returns 56

// this keyword - no longer bound with arrow functinos

const user = {
  name: 'Devon',
  cities: ['Memphis', 'Miramar Beach', 'Germantown'],
  // method on objects.
  printPlacesLived() {
    return this.cities.map((city) => this.name + ' has lived in ' + city);
  }
};

console.log(user.printPlacesLived());


// environment example

// const env = {
//   location: 'Miramar Beach',
//   terrain: 'Muddy',
//   precipChance: '10%',
//   temp: 34
// }

// console.log('Location: ' + env.location);
// console.log('Temperature: ' + env.temp + ' degrees');
// console.log('Chance of Rain: ' + env.precipChance);

// const weather = (x) => {
//   return x.temp;
// };
//
// const weatherConditions = {
//   temp: 43,
// };
//
// console.log(weather(weatherConditions));

// challenge area
const multiplier = {
  numbers: ['4','2','1'],
  multiplyBy: 2,
  multiply() {
    return this.numbers.map((number) => this.multiplyBy * number);
  }
};

console.log(multiplier.multiply()); //[CHECK/DONE]
