// es5 functions
// const square = function(x) {
//   return x * x;
// }
// console.log(square(3));

// es6 arrow functions
  // pattern: const definedVar = (vals) => {};

// const squareArrow = (x) => {
//   return x * x;
// };
// console.log(squareArrow(9));

// es6 arrow functions are always anonymous
const squareArrow = (x) => x * x;
console.log(squareArrow(5));

// challenge outline
  // Use Arrow Functions
  // =========================
  // getFirstName
  // first name logic from let-const
    // firstName = fullName.split('')[0]

const name = 'Devon Kiss';
// // full arrow function
// const regArrow = (x) => {
//   return x.split(' ')[0];
// }
// console.log(regArrow(name));

const smallArrow = (x) => x.split(' ')[0];
console.log(smallArrow(name));

// Yay-I-win.jpg, one-try.png 1!!!1!!11!
