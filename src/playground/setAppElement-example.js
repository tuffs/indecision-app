// File: IndecisionApp.js
//...

  // set prop: ariaHideApp
  <OptionModal
    ariaHideApp={false}
    selectedOption={this.state.selectedOption}
  />

//...
// EOF

// File: OptionModal.js
//.. after your Modal import statement

  Modal.setAppElement('#app'); // set to app element name in index.html
  const OptionModal = (props) => (...);

  export default OptionModal;
// EOF
