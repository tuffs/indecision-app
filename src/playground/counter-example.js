console.log('Counter Example with State Management (re-render)');

class Counter extends React.Component {
  constructor(props){
    super(props);
    this.handleAddOne = this.handleAddOne.bind(this);
    this.handleMinusOne = this.handleMinusOne.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.state = {
      count: 0
    };
  }
  // lifecylce methods (only class based components)
  componentDidMount() {
    try {
      const stringCount = localStorage.getItem('count');
      const count = parseInt(stringCount, 10);

      if (!isNaN(count)) {
        this.setState(() => ({ count }));
      }
    } catch (e) {
      // Do nothing at all.
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.count !== this.state.count){
      localStorage.setItem('count', this.state.count);
    }
  }
  componentWillUnmount() {
    console.log('componentWillUnmount');
  }
  handleAddOne() {
    this.setState((prevState) => {
      return {
        count: prevState.count + 1
      };
    });
  }
  handleMinusOne() {
    if(this.state.count != 0){
      this.setState((prevState) => {
        return {
          count: prevState.count - 1
        }
      });
    }
  }
  handleReset() {
    this.setState(() => {
      return {
        count: 0
      };
    });
  }
  render() {
    return(
      <div>
        <h1>Count: {this.state.count}</h1>
        <button onClick={this.handleAddOne}>+1</button>
        <button onClick={this.handleMinusOne}>-1</button>
        <button onClick={this.handleReset}>reset</button>
      </div>
    );
  }
}

// default count prop on load in render call with -> count={2}
// added to the <Counter /> component

ReactDOM.render(<Counter />, document.getElementById('app'));



// let count = 0;

// const addOne = () => {
//   count++;
//   renderCounterApp();
// };

// const minusOne = () => {
//   count--;
//   renderCounterApp();
// }

// const setZero = () => {
//   count = 0;
//   renderCounterApp();
// }

// const renderCounterApp = () => {
//   const templateTwo = (
//       <div>
//         <h3>Counter: {count}</h3>
//         <button onClick={addOne}>+1</button>
//         <button onClick={minusOne}>-1</button>
//         <button onClick={setZero}>reset</button>
//       </div>
//   );

//   ReactDOM.render(templateTwo, appRoot);
// };

// renderCounterApp();
