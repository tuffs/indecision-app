var nameVar = 'Devon';
nameVar = 'Another Name';
console.log('nameVar:', nameVar);

let nameLet = 'Jen';
nameLet = 'Julie';
console.log('nameLet:', nameLet);

const nameConst = 'Frank';
console.log('nameConst:', nameConst);

// traditionally we will use const UNLESS
  // we need to redefine the variable
    // then we will use let

// var based variables are function scoped
  // in that it was created inside of a function
  // and cannot be accessed outside of the function

function getPetName() {
  let petName = 'Hal';
  // petName exists inside the scope of this function
  return 'The saved pet\'s name is: ' + petName;
}

// out of scope pet name is inside getPetName() fx
  // petName is nonexistant here.
  // if you create a const named petName below
const petName = getPetName();
  // this will now work
console.log(petName); // <---

// Block level scoping
var fullName = 'Devon Kiss';
let firstName;

if (fullName) {
  firstName = fullName.split(' ')[0];
  console.log(firstName);
}

// remember const is constant
  // && cannot be changed!!
