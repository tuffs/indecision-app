const app = {
  title: 'Build It: Visibility App',
  subtitle: 'Click the button below to toggle visibility of the example text.',
  exampleText: 'This is the example text for the first build-it example application. You can click the button above to toggle the visibility of this text.'
}

let visibility = 1;

const toggleVisibility = () => {
  if (visibility === 1) {
    visibility = 0;
    app.exampleText = '';
    render();
  } else {
    visibility = 1;
    app.exampleText = 'This is the example text for the first build-it example application. You can click the button above to toggle the visibility of this text.';
    render();
  }
}

const appRoot = document.getElementById('app');

const render = () => {
  const template = (
    <div>
      <h1>{app.title}</h1>
      <p>{app.subtitle}</p>
      <button onClick={toggleVisibility}>Toggle Visibility of Example Text</button>
      <p>{app.exampleText}</p>
    </div>
  )

  ReactDOM.render(template, appRoot);
};

render();
