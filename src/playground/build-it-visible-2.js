class Visibility extends React.Component {
  constructor(props){
    super(props);
    this.toggleVisibility = this.toggleVisibility.bind(this);
    this.state = {
      visibility: false
    };
  }
  toggleVisibility() {
    this.setState((prevState) => {
        return {
          visibility: !prevState.visibility
        }
    });
  }

  render() {
    return (
      <div>
        <h3>Build It: Visibility Project</h3>
        <button onClick={this.toggleVisibility}>
          {this.state.visibility ? 'Hide Details' : 'Show Details'}
        </button>
        {this.state.visibility && (
          <div>
            <p>This is the text to be able to see for the details.</p>
          </div>
        )}
      </div>
    );
  }
}

ReactDOM.render(<Visibility/>, document.getElementById('app'));
