class MyApp extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const title = 'My Application Name';
    const subtitle = 'Even more information about your application';
    const bodyText = 'This is some text for you to examine how this application is working out for you. I do not know what eles to put here...';
    return (
      <div>
        <Title title={title} subtitle={subtitle} />
        <Body bodyText={bodyText} />
      </div>
    );
  }
}

class Title extends React.Component {
  render() {
    return (
      <div>
        <h3>{this.props.title}</h3>
        <p>{this.props.subtitle}</p>
      </div>
    );
  }
}

class Body extends React.Component {
  render() {
    return (
      <div className="column">
        <div className="row">
          <div className="col-md-5 col-md-offset-4">
            <p className="lead">
              {this.props.bodyText}
            </p>
          </div>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<MyApp />, document.getElementById('app'));
