import React from 'react';

const Header = (props) => (
  <div className="header">
    <div className="container">
      <h3 className="header__title">{props.title}</h3>
      {props.subtitle && <h5 className="header__subtitle">{props.subtitle}</h5>}
    </div>
  </div>
);

Header.defaultProps = {
  title: 'EatOut Destin, FL'
};

export default Header;
